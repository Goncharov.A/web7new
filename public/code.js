window.addEventListener('DOMContentLoaded', function (event) {
let position = 0;
let slidesToShow = 4;
const slidesToScroll = 1;
if (document.body.offsetWidth < 1000) {
    slidesToShow = 2;
}
const container = document.querySelector('.slider-container');;
const track = document.querySelector('.slider-track');

const btnPrev = document.querySelector('.btn-prev');
const btnNext = document.querySelector('.btn-next');
const items = document.querySelectorAll(`.slider-item`);
const itemCount = items.length;
const itemWidth = container.clientWidth / slidesToShow;
const movePosition = slidesToScroll * itemWidth;

items.forEach((item) => {
    item.style.minWidth = `${itemWidth}px`;
});

btnNext.addEventListener('click', () => {
    const itemLeft = itemCount - (Math.abs(position) + slidesToShow * itemWidth) / itemWidth;
    
    position -= itemLeft >= slidesToScroll ? movePosition : itemLeft * itemWidth;

    setPosition();
    checkBtns();
});

btnPrev.addEventListener('click', () => {
    const itemsLeft = Math.abs(position) / itemWidth;

    position += itemsLeft >= slidesToScroll ? movePosition : itemsLeft * itemWidth;

    setPosition();
    checkBtns();
});

const setPosition = () => {
    track.style.transform = `translateX(${position}px)`;
};
const checkBtns = () => {
    btnPrev.disabled = position === 0;
    btnNext.disabled = position <= -(itemCount - slidesToShow) * itemWidth;
};

checkBtns();
});
